define(function ()
{
	var Log = function (txt)
	{
		var UseConsoleLog = false;
		try { UseConsoleLog = app.UseConsoleLog; } catch (e) { }
		if (UseConsoleLog)
		{
			var ok_logged = false;
			if (!ok_logged)
				try { app.WriteToLog(txt); ok_logged = true; } catch (e) { }
			if (!ok_logged)
				try { console.log(txt); ok_logged = true; } catch (e) { }
			if (!ok_logged)
				try { WScript.Echo(txt); ok_logged = true; } catch (e) { }
		}
	};

	return function (log_name)
	{
		var logger =
		{
			LinePrefix: log_name,

			Error: function (txt)
			{
				Log('ERROR ' + log_name + ': ' + txt);
			}

			, Debug: function (txt)
			{
				Log('DEBUG ' + log_name + ': ' + txt);
			}

			, Info: function (txt)
			{
				Log('INFO ' + log_name + ': ' + txt);
			}

			, Echo: function (txt)
			{
				Log(txt);
			}
		}
		return logger;
	};
});
