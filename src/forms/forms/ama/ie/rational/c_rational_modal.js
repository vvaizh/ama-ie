﻿define([
	  'forms/ama/ie/rational/c_rational'
	, 'forms/base/h_msgbox'
],
function (c_rational, h_msgbox)
{
	return function (model)
	{
		var res =
			{
				text: function (item) { return item && item.Текст ? item.Текст : 'кликните, чтобы указать текст..'; }
				, controller: function (item, onAfterSave)
				{
					var controller =
					{
						ShowModal: function (e)
						{
							e.preventDefault();
							var controller = c_rational();
							if (model && model.Rational)
								controller.SetFormContent(model.Обоснование_платежа);
							var bname_Ok = 'Сохранить';
							h_msgbox.ShowModal({
								controller: controller
								, width: 460
								, height: 270
								, id_div: 'cpw-forms-ama-ie-rational'
								, title: 'Обоснование платежа'
								, buttons: [bname_Ok, 'Отменить']
								, onclose: function (bname)
								{
									if (bname_Ok == bname)
									{
										model.Обоснование_платежа = controller.GetFormContent();
										onAfterSave();
									}
								}
							});
						}
					};
					return controller;
				}
			}
		return res;
	}
});
