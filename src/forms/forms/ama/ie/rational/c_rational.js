﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/ie/rational/e_rational.html'
],
function (c_binded, tpl, codec_xml, h_times, print_tpl, codec_tpl_xaml)
{
	return function ()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input').change(function () { self.UpdateChangeRadio(); })

			this.UpdateChangeRadio();
		}

		controller.UpdateChangeRadio = function ()
		{
			var sel = this.binding.form_div_selector;
			var div = $(sel + ' div.cpw-forms-ama-ie-rational');
			if ('checked' == $(sel + ' input').attr('checked'))
			{
				$(sel + ' textarea').attr('readonly', 'readonly');
				div.addClass('default');
				div.removeClass('custom');
			}
			else
			{
				$(sel + ' textarea').removeAttr('readonly');
				$(sel + ' textarea').focus();
				div.removeClass('default');
				div.addClass('custom');
			}
		}

		return controller;
	}
});
