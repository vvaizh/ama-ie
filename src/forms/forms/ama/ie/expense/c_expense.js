﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/ie/expense/e_expense.html'
	, 'forms/ama/ie/rational/c_rational_modal'
],
function (c_binded, tpl, c_rational_modal)
{
	return function ()
	{
		var options = 
		{
			field_spec: { Обоснование_платежа: c_rational_modal }
		};
		var controller = c_binded(tpl, options);

		return controller;
	}
});
