﻿define
(
	[
		  'forms/collector'
		, 'forms/ama/ie/expense/c_expense'
		, 'forms/ama/ie/rational/c_rational'
		, 'forms/ama/debtor/spouse/c_spouse'
		, 'forms/ama/debtor/ward/c_ward'
		, 'forms/ama/debtor/spouses/c_spouses'
		, 'forms/ama/debtor/wards/c_wards'
		, 'forms/ama/debtor/npextra/c_npextra'
	],
	function (collect)
	{
		return collect([
		  'expense'
		, 'rational'
		, 'debtor_spouse'
		, 'debtor_ward'
		, 'debtor_spouses'
		, 'debtor_wards'
		, 'debtor_npextra'
		], Array.prototype.slice.call(arguments, 1));
	}
);