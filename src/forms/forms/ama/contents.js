﻿define
(
	[
		  'forms/collector'

		, 'txt!forms/ama/ie/expense/tests/contents/test1.json.txt'
		, 'txt!forms/ama/debtor/spouses/tests/contents/test1.json.txt'
		, 'txt!forms/ama/debtor/wards/tests/contents/test1.json.txt'
		, 'txt!forms/ama/debtor/npextra/tests/contents/test1.json.txt'
	],
	function (collect)
	{
		return collect([
		  'expense_test1'
		, 'debtor_spouses_test1'
		, 'debtor_wards_test1'
		, 'debtor_npextra_test1'
		], Array.prototype.slice.call(arguments, 1));
	}
);