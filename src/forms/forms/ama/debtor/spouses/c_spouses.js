﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/debtor/spouses/e_spouses.html'
	, 'forms/base/b_collection'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/h_msgbox'
	, 'forms/ama/debtor/spouse/c_spouse'
],
function (c_binded, tpl, b_collection, copy_codec, h_msgbox, c_spouse)
{
	return function ()
	{
		var options =
			{
				CreateBinding: b_collection
				, ccopy: copy_codec()
				, h_msgbox: h_msgbox
				, c_item: c_spouse
			};
		var controller = c_binded(tpl, options);
		return controller;
	}
});
