﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/debtor/spouse/e_spouse.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);
		controller.width = 350;
		controller.height = 350;
		return controller;
	}
});
