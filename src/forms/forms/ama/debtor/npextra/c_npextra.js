﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/debtor/npextra/e_npextra.html'
	, 'forms/ama/debtor/wards/c_wards'
	, 'forms/ama/debtor/spouses/c_spouses'
],
function (c_binded, tpl, c_wards, c_spouses)
{
	return function ()
	{
		var options = {
			field_spec:
			{
				Супруги: { controller: c_spouses }
				, Иждивенцы: { controller: c_wards }
			}
		};
		var controller = c_binded(tpl, options);
		return controller;
	}
});
