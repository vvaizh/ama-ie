﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/ama/debtor/ward/e_ward.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);
		controller.width = 360;
		controller.height = 270;
		return controller;
	}
});
